# switch key bind
unbind C-b
set -g prefix C-s
bind C-s send-prefix

# remove escape delay when running vim in tmux
set -s escape-time 0

# set up the colors
set -g default-terminal "screen-256color"

# Status bar
set -g status-keys "emacs"
set -g status-fg white
set -g status-bg default
set -g status-justify centre
set -g status-left-length 20
set -g status-left "#[fg=cyan][CPU:#{cpu_icon}GPU:#{gpu_icon}]#[fg=default][#{prefix_highlight}#{download_speed}]"
set -g status-right "[#{battery_icon} #{battery_percentage} #{battery_remain}] #[fg=cyan][#[fg=green]%m/%d #[fg=cyan]%H:%M]"
setw -g window-status-current-format '#[fg=red,bg=default]( #I:#W )#[fg=default]'

# 0 is too far from 1 :)
set -g base-index 1
setw -g pane-base-index 1

# splits and movements (most managed by tmux-pain-control)
bind u previous-window
bind i next-window
bind \\ split-window -h
bind | split-window -h -c "#{pane_current_path}"
bind - split-window -v -c "#{pane_current_path}"
bind _ split-window -v

# mouse type things
set-option -g bell-action any
set-option -g visual-bell off
setw -g monitor-activity on

# useful shortcuts
bind r source-file ~/.tmux.conf\; display-message " Config reloaded.."

#bind j command-prompt "join-pane -s :'%%'"

# make copy and paste similar to vim
setw -g mode-keys vi
unbind [
unbind p
bind C-y copy-mode
bind p paste-buffer
bind -Tcopy-mode-vi v send -X begin-selection

# plugins
set -g @plugin 'tmux-plugins/tpm'
# set -g @plugin 'tmux-plugins/tmux-copycat'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-prefix-highlight'
set -g @plugin 'tmux-plugins/tmux-pain-control'
set -g @plugin 'tmux-plugins/tmux-battery'
set -g @plugin 'tmux-plugins/tmux-net-speed'
set -g @plugin 'tmux-plugins/tmux-cpu'

# Initialize TMUX plugin manager
# (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'
