# Aliases File for all shells

## System Aliases
# Reset swap space
alias swap-reset="sudo swapoff -a; sudo swapon -a"

## Git Aliases
alias gu="git pull"	
alias gst="git status"
alias gl="git log --decorate --graph --pretty=short"
alias ga="git add"
alias gm="git merge"
alias gc="git commit -m"
alias gp="git push"	
alias gpd="git push -u origin develop"
alias gpm="git push -u --tags origin master"			
alias gpd="git push -u origin develop"
alias gch="git checkout"
alias gb="git branch"
alias gchb="git checkout -b"
alias gsubup="git submodule update --recursive"

## Mercurial Aliases
alias hu="hg pull"
alias hst="hg status"
alias hc="hg commit -m"
alias hp="hg push"
alias hup="hg update"

## Goff/Hoff Aliases
alias goffhoffpush="goff push; hoff push"
alias goffhoffstatus="goff status; hoff status"

## Shell operations
# Destroy contents of build directory while inside
alias build_clean="y rm -r ../build/*"
# List all items
alias lsa="ls -a"
# Move to parent directory
alias ..="cd .."
# Move to previous directory
alias back="cd -"
# Pipe `y` to something
alias y="echo y |"
# Exit
alias x="exit"
# Call valgrind with usual flags
alias vgrind="valgrind --leak-check=full --error-limit=no --gen-supressions=all"
# Delete all .orig files
alias del_orig="find . -name '*.orig' -delete "
# Use forked neofetch in this shell
alias neofetch="bash ~/env/deps/neofetch/neofetch"

## Program aliases
make_program_alias tmux t			# Tmux
make_program_alias python py		# Python
make_program_alias python3 py3		# Python3
make_program_alias nvim vim			# NeoVim
make_program_alias vim vi			# Whatever vim is mapped to
make_program_alias taskminder tm	# Taskminder

## Qemu
alias q_create_disk="qemu-img create -f qcow2"
alias q_boot_leopard_ppc="qemu-system-ppc -L pc-bios -boot c -M mac99,via=pmu -m 1024 -prom-env -hda /home/b/qemu/ppc-leopard-disk.img"

## Compiler Operations
alias remake="make clean && make"
alias compile_c="gcc -o"

## Python Aliases
alias pypi_clean_distribution="rm -rfv build/ dist/ *.egg-info/"
alias pypi_build_wheel="python3 setup.py sdist bdist_wheel"
alias pypi_publish_wheel="python3 -m twine upload -r pypi dist/*"
alias conda="~/anaconda3/bin/conda"

## Compiler Specification
# TODO: make an alias program for this
make_compiler_alias gcc CC CC CC
make_compiler_alias g++ CXX CXX CXX
make_compiler_alias gfortran F90 F77 FC
make_compiler_alias icc CC CC CC
make_compiler_alias icpc CXX CXX CXX
make_compiler_alias ifort F90 F77 FC
make_compiler_alias clang CC CC CC
make_compiler_alias clang++ CXX CXX CXX
make_compiler_alias pgcc CC CC CC
make_compiler_alias pgc++ CXX CXX CXX
make_compiler_alias pgfortran F90 F77 FC

## IP Addresses
# Get public IP address
alias ipp="curl ipecho.net/plain; echo;"

## Weather
#sheridan, WY weather
alias weather-shr="curl wttr.in/82801"
# Red Lodge, MT weather
alias weather-rl="curl wttr.in/59068"
# Billings, MT weather
alias weather-bil="curl wttr.in/59108"
# Missoula, MT weather
alias weather-mso="curl wttr.in/59802"

## Just for fun
# The magic word
alias please="sudo"
# Best way to rick roll someone
alias rick-roll="curl -s -L http://bit.ly/10hA8iC | bash"
# Watch a movie
alias starwars="telnet towel.blinkenlights.nl"
# Play chess
alias chess="telnet freechess.org"
# Pretend people other than me still use telnet
alias telehack="telnet telehack.com"
# Delete with my usual flags
alias yeet="rm -rv"

# Path modifications
export PATH="$PATH":"$HOME/.pub-cache/bin"

# Other variables
export DOTNET_CLI_TELEMETRY_OPTOUT=1

