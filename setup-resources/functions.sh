## Functions for interactive setup

# Upgrade an installed package
function upgrade {
	package=$1
	pw=$2
	if command -v brew > /dev/null
	then
		brew upgrade $package
	elif command -v apt > /dev/null
	then
		sudo apt-get -q upgrade $package
	elif command -v pacman > /dev/null
	then
		sudo pacman -S $package >> $log_file
	elif command -v dnf > /dev/null
	then
		sudo dnf install $package
	elif command -v zypper > /dev/null
	then
		echo y | sudo zypper -n update $package
	elif command -v yum > /dev/null
	then
		sudo yum -y install $package
	elif command -v pkg_add > /dev/null
	then
		sudo pkg_add -v $package
	fi
}

# Install or upgrade a package
function install {
	package=$1
	pw=$2
	if command -v $package > /dev/null
	then
		upgrade $package $pw
	elif command -v brew > /dev/null
	then
		brew install $package
	elif command -v apt > /dev/null
	then
		sudo apt-get -q install $package
	elif command -v pacman > /dev/null
	then
		sudo pacman -S $package >> $log_file
	elif command -v dnf > /dev/null
	then
		sudo dnf install $package
	elif command -v zypper > /dev/null
	then
		sudo zypper -n install $package
	elif command -v yum > /dev/null
	then
		sudo yum -y install $package
    elif command -v pkg_add > /dev/null
	then
			sudo pkg_add -v $package
	else
		echo "UNSUPPORTED OS\n"
	fi
}

# Install a package via snap
function snap_install {
    package=$1
	if command -v snap > /dev/null;
	then
		sudo snap install $package
	fi
}

# Copy a file and hide
function cphidden {
	file=$1
	path=$2
	cp -v $file $path/.$file
}

# Delete path if it exists
function delete_if_exists {
    path=$1
    if [ -e $path ]
	then
		rm -rfv $path
    elif [ -d $path ]
	then
		rm -rfv $path
	fi
}

# Configure git
function git_config {
	un_prompt="Enter user name for Git: "
	em_prompt="Enter email address for Git: "
	
	read -p "Enter username for Git: " name
	read -p "Enter email address for Git: " email
	
	git config --global user.name "$name"
	git config --global user.email "$email"
	git config --global core.excludesfile ~/env/git/gitignore_global
	git config pull.rebase false
	
	if command -v vimdiff > /dev/null
	then
		git config merge.tool vimdiff
		git config merge.conflictstyle diff3
	fi
}

# Configure Mercurial
function hg_config {
	cp -v hg/hgrc ~/.hgrc
}

# Install my own script
function my_install {
	script=$1
	new_name=$2

	if [ -e apps/$script ]
	then
		echo "Installing $new_name"
		cp -v apps/$script ~/bin/$new_name
	else
		echo "Unable to find $script"
	fi

	if [ -e ~/bin/$new_name ]
	then
		chmod +x ~/bin/$new_name
	fi
}

# Copy systemwide configuration
function copy_systemwide {
	sudo cp -v system/etc_profile /etc/profile
	sudo cp -v system/etc_zshrc /etc/zshrc
	sudo cp -v last-update /etc/last-update
}

# Prompt user to select packages for installation
function install_packages {
	sel_packages
	clear
	for pkg in ${install_list[@]}
	do
		install $pkg
	done
	if command -v brew > /dev/null		# macOS only packages
	then
		sel_packages_mac
		for pkg in ${install_list_mac[@]}
		do
			install $pkg
		done
	elif command -v apt > /dev/null		# Debian only packages
	then
		for pkg in ${packages_deb[@]}
		do
			install $pkg $pw
		done
	elif command -v pacman > /dev/null	# Arch only packages
	then
		install traceroute $pw
	fi
}

# Select and install packages via snap
function install_snap_packages {
    sel_packages_snap
	clear
	for pkg in ${install_list[@]}
	do
		snap_install $pkg
	done
}

# Configure Version control
function vc_config {
	if command -v git > /dev/null
	then
		sleep 2
		git_config	
	elif command -v hg > /dev/null
	then
		hg_config
	fi
}

# Configure Hyper
function hyper_config {
	echo "Configuring Hyperterm."
	echo "Old hyperterm config can be found at ~/.hyper.js.bak"
	mv -v $HOME/.hyper.js $HOME/.hyper.js.bak
	cp -rv hyper/hyper.js $HOME/.hyper.js
	sleep 2
}

# Configure Vim/NeoVim
function vim_config {
	echo "Configuring Vim"
	cp -v vim/vimrc ~/.vimrc
	cp -v vim/vim-conf ~/.vim-conf

	install vim-pathogen
	mkdir -p ~/.vim/autoload ~/.vim/bundle && \
	curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

   	cp -rv deps/nerdtree ~/.vim/bundle
   	cp -rv deps/vim-airline ~/.vim/bundle
	cp -rv deps/vim-gitgutter ~/.vim/bundle
	cp -rv deps/vim-visual-multi ~/.vim/bundle
	if [ -d ~/.vim/bundle/vim-multiple-cursors ]
	then
		echo "Deleting old vim multiple cursrs plugin"
		rm -rfv ~/.vim/bundle/vim-multiple-cursors
	fi
}

# Copy user bash configuration
function copy_bash {
	cp -v bash/bash_profile ~/.bash_profile
	cp -v bash/bashrc ~/.bashrc
	mkdir ~/.bash/
	cp -v bash/git-prompt.sh ~/.bash/git-prompt.sh
	cp -v bash/ps1 ~/.bash/ps1
}

# Copy user Zsh configuration
function copy_zsh {
	cp -v zsh/zshrc ~/.zshrc
}

# Copy user fish configuration
function copy_fish {
	if command -v omf > /dev/null;
	then
			omf install bira
	fi

	cp -v fish/config.fish ~/.config/fish/config.fish
}

# Copy user's own scripts
function copy_own_scripts {
	my_install tmp-clean.sh tmp-clean
    my_install unzip_all_recursive.sh unzip_all_recursive
}

# Copy user tmux configuration
function copy_programs {
	cp -v tmux/tmux.conf ~/.tmux.conf
	if [ -d ~/.config/htop ]
	then
		cp -v htop/htoprc ~/.config/htop
	fi
}

# Install zsh syntax highlighting
function zsh_syntax {
	if [ ! -d ~/.zsh-syntax-highlighting ]
	then
		cp -rv repos/zsh-syntax-highlighting/ ~/.zsh-syntax-highlighting/
	fi
}

# Install Oh My Zsh
function oh_my_zsh {
	sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" "" --unattended
    cp -v zsh/bk.zsh-theme ~/.oh-my-zsh/themes
}

# Install Oh My Fish
function oh_my_fish {
	curl -L https://get.oh-my.fish | fish --noninteractive
}

# Clean shell environment
function clean_env {
    delete_if_exists ~/.ssh-aliases
	delete_if_exists ~/.bash*
	delete_if_exists ~/.zshrc
	delete_if_exists ~/.oh-my-zsh
	delete_if_exists ~/.config/htop/htoprc
	delete_if_exists ~/.tmux.conf
	delete_if_exists ~/bin/tmp-clean
    delete_if_exists ~/.vimrc
	delete_if_exists ~/.vim-conf
	delete_if_exists ~/.vim/autoload
    delete_if_exists ~/.vim/bundle
	delete_if_exists ~/.config/nvim/autoload
	delete_if_exists ~/.config/nvim/bundle
	delete_if_exists ~/.config/nvim/init.vim
	delete_if_exists /etc/profile
	delete_if_exists /etc/zshrc
	delete_if_exists /etc/last-update
	delete_if_exists ~/env
}

# Display user input options
function user_menu {
	if command -v dialog > /dev/null;
	then
		input=$(dialog --menu "Select an option:" 25 60 17 \
			1 "Install Fish" \
			2 "Install Oh My Fish" \
			3 "Make Fish default shell" \
			4 "Install Z Shell" \
			5 "Install Oh My Zsh" \
			6 "Make Z Shell default shell" \
			7 "Install Packages" \
			8 "Configure Version Control" \
			9 "Configure Vim and NeoVim" \
			10 "Copy Systemwide Config" \
			11 "Copy Bash Config" \
			12 "Copy Z Shell Config" \
			13 "Copy Fish Config" \
			14 "Install bundled Scripts" \
			15 "Copy program configuration" \
			16 "Install ZSH Syntax Highlighting" \
			17 "Configure Hyperterm" \
			0 "Exit" \
			3>&1 1>&2 2>&3 3>&-)
	else
		echo ""
		echo "----------------------------------------------------"
		echo "Interactive shell configuration"
		echo "1:	Install Fish"
		echo "2:	Install Oh My Fish"
		echo "3:	Make Fish default shell"
		echo "4:	Install Z Shell"
		echo "5:	Install Oh My Zsh"
		echo "6:	Make ZSH default shell"
		echo "7:	Install Packages"
		echo "8:	Configure VCS"
		echo "9:	Configure Vim and NeoVim"
		echo "10:	Copy systemwide config (requires Sudo)"
		echo "11:	Copy bash config"
		echo "12:	Copy zsh config"
		echo "13:	Copy Fish config"
		echo "14:	Install bundled scripts"
		echo "15:	Copy program configuration"
		echo "16:	Install ZSH Syntax Highlighting"
		echo "17:	Configure Hyperterm"
		echo "18:	Clean environment"
		echo "0:	Exit"
		echo "----------------------------------------------------"
		echo ""
		read -p "Enter option: " input
	fi
}

# Main configure menu
function config_menu {
	while :
	do
		user_menu

		if (( input == 1 )) # Install fish
		then
				install fish
		elif (( input == 2 ))	# Install oh my fish
		then
				oh_my_fish
		elif (( input == 3 ))	# Make fish default shell
		then
				chsh -s $(which fish)
		elif (( input == 4 ))	# Install zsh
		then
			install zsh
		elif (( input == 5 ))	# Install Oh My Zsh
		then
			oh_my_zsh
		elif (( input == 6 ))	# Make ZSH default shell
		then
			chsh -s /bin/zsh	
		elif (( input == 7 ))	# Install Packages
		then
			install_packages
		elif (( input == 8 ))	# Version Control
		then
			vc_config
		elif (( input == 9 ))	# Vim
		then
			vim_config
		elif (( input == 10 ))	# Systemwide config
		then
			copy_systemwide
		elif (( input == 11 ))	# Bash
		then
			copy_bash
		elif (( input == 12 ))	# Zsh
		then
			copy_zsh
		elif (( input == 13 ))	# Fish
		then
			copy_fish
		elif (( input == 14  ))	# My scripts
		then
			copy_own_scripts
		elif (( input == 15 ))	# shell programs
		then
			copy_programs
		elif (( input == 16 ))	# ZSH Syntax Highlighting
		then
			zsh_syntax
		elif (( input == 17 ))	# Hyperterm
		then
			hyper_config
	    elif (( input == 18 ))	# Clean environmet
		then
		    clean_env
		elif (( input == 0 ))	# Exit script
		then
			clear
			exit 0
		else
			echo "Invalid Entry."
		fi

    sleep 2

	done
}

# Configure single user account
function acct_config {

	if command -v fish > /dev/null
	then
		chsh -s $(which fish)
		copy_fish
	fi

	if command -v zsh > /dev/null;
	then
		if command -v fish > /dev/null
		then
			echo ""
		else
			chsh -s $(which zsh)
		fi
		zsh_syntax
		oh_my_zsh
		copy_zsh
	fi

	copy_bash
	copy_own_scripts
	copy_tmux
}
