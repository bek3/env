#!/usr/bin/env bash

# Sync SSH Aliases
# Brendan Kristiansen

function sync {
	cd ~/.ssh-aliases
	git pull
	git add .
	git commit -m "Sync from $(hostname)"
	git push -u origin master
	cd -
}

function clone {
	url=$1
	cd ~
	git clone $url $/.ssh-aliases
	cd -
}

if [ -d ~/.ssh-aliases ]
then
	if [ -d ~/.ssh-aliases/.git ]
	then
		sync
	else
		echo "Alias folder not set up for sync."
		read -p "Enter URL for sync repository: " url
		cd ~/.ssh-aliases
		git init
		git remote add origin $url
		git pull origin master
		git add .
		git commit -m "Initial sync for $(hostname)"
		git push -u origin master
		cd -
	fi
else
	read -p "No SSH alias sync folder found. would you like to initialize one? \
			(y/n): " input
	if (( input == 'y' ))
	then
		echo "This requires you have created a Git repository to track alias \
				files. Press <C-c> if you have not."
		read -p "Enter URL to clone: " url
		clone $url
	fi
fi
