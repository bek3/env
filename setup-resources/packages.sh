## Packages to install during setup

function sel_packages {
	if command -v dialog > /dev/null;
	then
		install_list=$(dialog --checklist "Select Packages to install:" 25 65 19 \
		tmux "Terminal Multiplexor" off \
    	cowsay "Cowsay" off \
		fortune "Fortune off \"
		dialog "Dialog" off \
		ag "Silver Searcher" off \
        wget "Wget" off \
		htop "Htop" off \
		p7zip "7Zip" off \
		nmap "Network Map" off \
		tree "Tree" off \
		ranger "Ranger" off \
		git "Git" off \
		hg "Mercurial" off \
		vagrant "vagrant" off \
		clang-format "Clang Format" off \
		clang "Clang" off \
		gcc "GCC" off \
		g++ "G++" off \
		gfortran "GNU Fortran" off \
		valgrind "Valgrind" off \
		composer "Composer" off \
		python3 "Python 3" off \
		cython "Cython" off \
		telnet "Telnet" off \
		imagemagick "ImageMagick" off \
		sqlite3 "SQLite 3" off \
		cockpit "Cockpit" off \
		snap "Snap" off \
		libreoffice "Libreoffice" off \
		gnuplot "GNU Plot" off \
		octave "GNU Octave" off \
		google-earth-pro-stable "Google Earth Pro" off \
		3>&1 1>&2 2>&3 3>&-)
	else
		install_list=$packages
	fi
}

function sel_packages_snap {
    if  command -v snap > /dev/null;
	then
	    if command -v dialog > /dev/null;
	    then
	    	install_list=$(dialog --checklist "Select Packages to install:" 25 65 6 \
			multipass "Ubuntu Multipass" off \
			spotify "Spotify" off \
			vlc "VLC" off \
			slack "Slack" off \
			skype "Skype" off \
			firefox "Firefox" off \
	    	3>&1 1>&2 2>&3 3>&-)
	    else
	    	install_list=$packages
	    fi
	fi
}

function sel_packages_mac {
	if command -v dialog > /dev/null
	then	
		install_list_mac=$(dialog --checklist \
				"(macOS only) Select Packages to install:" 10 35 5 \
		cask "Homebrew Cask" off \
		3>&1 1>&2 2>&3 3>&-)
	else
		install_list_mac=$packages_mac
	fi
}

function get_pkg_for_index {
	idx=$1
	echo $packages[$idx]
}

packages=(
	tmux
	dialog
	ag
	wget
	htop
	p7zip
	nmap
	tree
	neovim
	git
	composer
	mercurial
	gcc
	g++
	gfortran
	imagemagick
	cockpit
	)

packages_mac=(
	cask
	)

packages_deb=(
	traceroute
	)
