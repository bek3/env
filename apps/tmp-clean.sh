# Clean User's tmp directory
# Brendan Kristiansen
# Created on 3 August 2018

#!/usr/bin/env zsh

name=$(whoami)

if [ -d ~/tmp/ ]
then
	echo "Cleaning tmp directory for $name"
	rm -rfv ~/tmp/*
else
	echo "No tmp directory for $name"
fi
