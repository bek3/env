# unzip_all_recursive
# Brendan Kristiansen

#!/usr/bin/env bash

if [ -e ~/env/aliases ]
then
		source ~/env/aliases
else 
		echo "Requisite aliases unable to be sourced."
		exit -1
fi

for itm in ./*;
do
		if [ -d $itm ]
		then
				cd $itm
				unzip_all_recursive
				cd ..
		fi
		unzip_all
done
