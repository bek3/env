# Terminal Environment

**VERSION 2.9.0**

Documentation updated on 30 October 2021 (version 2.9.0)

Project website: http://www.bek.sh/software/env.html

## Interface

The interactive setup is compatible with systems that have `dialog` installed.
While a plaintext version of setup is maintained, It is strongly suggested you
install dailog, as it gives you more control over the setup process.

## What's Installed

### Programs

The following programs are optionally installed during configuration.

* Tmux	     
* Dialog
* Silver Searcher
* Wget
* Htop
* 7zip
* Nmap
* Tree
* Ranger
* Neovim
* Git
* Mercurial
* Vagrant
* Clang Format
* Clang, Clang++
* GCC, G++, GFortran
* Valgrind
* Composer
* Python3, Pip 3
* Cython
* Telnet
* ImageMagick
* SQLite 3
* Cockpit
* Libreoffice
* GNU Plot
* GNU Octave
* Google Earth Pro

### Vim plugins

The following plugins are installed for use in Vim and NeoVim. My environment
uses Pathogen to install plugins.

| Plugin								| Description							|
|:----------------------------------------------------------------------|:--------------------------------------------------------------|
|[Vim Gitgutter](https://github.com/airblade/vim-gitgutter)		| Show git diff in line numbers.				|
|[Nerdtree](https://github.com/scrooloose/nerdtree)			| Navigate directory tree while editing. Use `<C-t>` to toggle.	|
|[Vim Airline](https://github.com/vim-airline/vim-airline)		| Sweet status bar for Vim.					|
|[Multiple Cursors](https://github.com/terryma/vim-multiple-cursors)	| Stolen from Sublime.						|

## Version Control Configuration

### Git

Prompts user for name and email address and sets input data in global config.
Also links the global gitignore in this repository.

### Mercurial

Copies .hgrc to home directory. Requires user to update information manually.

## Clean Environment

Cleans any config files that have been installed or modified by this package.

## Windows Command Prompt

Files to make a Microsoft command prompt have been included as well. Run
`windows/install.bat` with Administrator privileges to give your command prompt
`ls` and `rm` commands, as well as a few other aliases to make UNIX users feel
a little more at home.

## Acknowledgements

This configuration optionally utilizes Robby Russell's [Oh My Zsh](https://github.com/robbyrussell/oh-my-zsh)
and is based loosely on [Dave Millman's configuration](https://github.com/dlm/env).
Global gitignore was compiled from [here](https://github.com/github/gitignore). The neofetch used in this project
is my own fork of the original script located [here](https://github.com/dylanaraps/neofetch), licensed under the MIT
license.

## Changelog

To view the history of this code, see the repository's commit history
[here](https://bitbucket.org/bek7/env/commits).

Prior to version 2.0.0 released on 09 July 2019, major changes were marked by
tags with dates.

