# Automatically source extra files like SSH aliases

if [ -a ~/.ssh-aliases ]
then
	for system in ~/.ssh-aliases/*.alias;
	do
		source $system
	done
fi

# POSIX-only aliases

# Switch SSH key
alias switch_ssh="eval '$(ssh-agent)'; ssh-add"

# Unzip all files here with 7zip
alias unzip_all="for item in ./*; do 7z x $item; done"

