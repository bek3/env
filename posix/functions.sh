# Function definitions for posix-compliant shells

function make_program_alias {
	prgm=$1
	alias=$2
	if command -v $prgm > /dev/null;
	then
		alias $alias="$prgm"
	fi
}

function make_compiler_alias {
    prgm=$1
	variable=$2

	if command -v $prgm > /dev/null;
	then
		alias use_$prgm="export $variable=$(which $prgm); \
						export $3=$(which $prgm); \
						export $4=$(which $prgm)"
	fi
}

