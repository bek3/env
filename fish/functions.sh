# Function definitions for the Friendly Interactive Shell

function make_program_alias --argument-names 'prgm' 'alias'
	# Make an alias to an installed program

	if command -v $prgm > /dev/null;
		alias $alias="$prgm"
	end
end

function make_compiler_alias
	# Set compiler aliases prior to builds

	set prgm $argv[1]
	if command -v $prgm > /dev/null
		alias use_$prgm="export $variable=(which $prgm); \
						export $argv[2]=(which $prgm) \
						export $argv[3]=(which $prgm)"
	end
end

function add_to_fish_path --argument-names 'loc'
	# Add a directory to your path

	set -U fish_user_paths $fish_user_paths $loc
	echo "Added $loc to Fish path"
end

