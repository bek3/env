source ~/env/fish/functions.sh
source ~/env/aliases

if test -e ~/env/aliases-local
	source ~/env/aliases-local
end

if test -e ~/.ssh-aliases
	for system in ~/.ssh-aliases/*.alias
		source $system
	end
end

if test -e ~/env/aliases-local
	source ~/env/aliases-local
end

alias switch_ssh="eval '(ssh-agent)'; ssh-add"
alias unzip_all_7zip="for item in ./*; 7z x $item; end;"

bash ~/env/deps/neofetch/neofetch

