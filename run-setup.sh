#!/usr/bin/env bash

# Interactive setup for shell
# Brendan Kristiansen

source setup-resources/functions.sh
source setup-resources/packages.sh

log_file=log.txt
dialog_backtitle="Interactive Shell Configuration"
version=$(cat env-version)

if command -v dialog > /dev/null;
then
	dialog --title 'Interactive Setup' --msgbox \
			'Welcome to Interactive Terminal Configuration' 6 50
else
	echo "For interactive setup, it is strongly recommended you install \
			Dialog for maximum control over the configuration process."
	read -p "Would you like to install dialog? [Y/n] " install_dialog
	if (( install_dialog == 'Y' ))
	then
		install dialog
	else
		echo "Proceeding without dialog..."
	fi
fi

# Install homebrew if necessary
if command -v brew >> /dev/null
then
	echo "Checking for Homebrew updates..."
	brew update
else
	if [ $(uname) == "Darwin" ]
	then
		/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	fi
fi

# Check for sudo
if command -v sudo > /dev/null
then
		echo "Found Sudo"
else
		echo "Sudo not found. This could cause issues when installing packages"
		sleep 3
fi

# Create tmp and bin directories if doesn't exist
if [ ! -d $HOME/tmp/ ]
then
	echo "Creating Tmp"
	mkdir -v $HOME/tmp/
fi
if [ ! -d $HOME/bin/ ]
then
	echo "Creating Bin"
	mkdir -v $HOME/bin/
fi

# Create local aliases
if [ ~ -e ~/env/aliases-local ]
then
	echo "# Aliases to be ignored by Git" > ~/env/aliases-local
fi

# Main setup interface
while :
do
	if command -v dialog > /dev/null;
	then
	input=$(dialog --menu "Select an option:" 20 60 9 \
			1 "Enter interactive setup" \
			2 "Minimal setup" \
			3 "Set up for development" \
			4 "Configure Individual Account" \
			5 "Clean Environment" \
			6 "Select Snap packages to install" \
			7 "Update Epel Repository (Red Hat only)" \
			0 "Exit" \
			3>&1 1>&2 2>&3 3>&-)

			if (( input == 1 ))		# Interactive
			then
				config_menu
			elif (( input == 2 ))	# Complete minimal setup
			then
				install tmux

				copy_tmux
				copy_bash
			elif (( input == 3 ))	# Install developer tools
			then
				install gcc
				install g++
				install gfortran
				install clang
				install clang-format
				install valgrind
				install php
				install octave
				install composer
				install ruby
				install python2
				install python3
				install git
				install mercurial
				install tmux
				install ag
				install neovim
				install cmake

				vcs_config
				vim_config
				copy_bash
				copy_tmux
			elif (( input == 4 ))	# Configure user account
			then
				acct_config
		    elif (( input == 5 ))
		    then
				clean_env
		    elif (( input == 6 ))
			then
				install_snap_packages
		    elif (( input == 7 ))
	 	    then
				sudo yum install epel-release
    		elif (( input == 0 ))	# Exit
			then
				clear
				exit
			fi
	else
		config_menu
	fi
	

done
# Main loop
