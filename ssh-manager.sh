#!/usr/bin/env bash

# Manage SSH aliases
# Brendan Kristiansen
# Part of shell environment

function menu {
	echo "Enter an option"
	echo " 1:	Create SSH alias"
	echo " 2:	Delete SSH alias"
	echo " 3:	List SSH aliases"
	echo " 4:	Sync SSH aliases"
	echo " 5:	Clear SSH aliases"
	echo " 0:	Exit"
}

function check_sync_dir {
	if [ ! -d ~/.ssh-aliases ]
	then
		echo "No sync directory found. Creating..."
		mkdir -v ~/.ssh-aliases
	fi
}

if command -v ssh >> /dev/null
then
	ssh -V
else
	echo "SSH not installed on system."
	exit -1
fi

check_sync_dir
cd ~/.ssh-aliases

while :
do
	menu
	read -p "Select option: " input
	if (( input == 1 ))
	then
		read -p "Enter new system name: " name
		read -p "Enter address for new system: " addr
		read -p "Enter login name for new system: " username
		read -p "Enter port to connect over: (22 is standard): " port

		alias_cmd="ssh $username@$addr -p $port"
		alias_file="$name.alias"
	
		if [ ! -e $alias_file ]
		then
			echo "alias to_$name='$alias_cmd'" > $alias_file
		else
			echo "Alias found with same name:"
			cat $alias_file
			read -p "Would you like to overwrite? (y/n): " input
			if (( input == 'y' ))
			then
				rm -v $alias_file
				echo "alias to_$name='$alias_cmd'" > $alias_file
			else
				echo "Retaining original file."
			fi

		fi

		read -p "Would you like to copy ssh keys (1 = yes, 2 = no)?" keys
		if (( keys == 1 ))
		then
			ssh-copy-id $username@$addr -p $port
		fi
	elif (( input == 2 ))
	then
		read -p "Enter system name to delete: " input
		rm -fv $input.alias
	elif (( input == 3 ))
	then
		ls
	elif (( input == 4 ))
	then
		bash ~/env/setup-resources/ssh-sync.sh
	elif (( input == 5 ))
	then
		echo "Deleting SSH aliases..."
		rm -rfv ~/.ssh-aliases
	elif (( input == 0))
	then
		cd -
		exit
	else
		echo "Invalid Option."
	fi
done
